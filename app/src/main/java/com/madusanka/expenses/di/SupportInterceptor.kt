package com.example.foody.di

import okhttp3.*

/**
 * Created by Chathura Wijesinghe on 23/4/19.
 * Elegantmedia
 * chaturaw.emedia@gmail.com
 */

class SupportInterceptor(private val apiKey: String) : Interceptor, Authenticator {
    var accessToken: String? = null
    private var callback: AuthenticatorCallBack? = null

    override fun intercept(chain: Interceptor.Chain): Response {
        val request: Request.Builder = chain.request().newBuilder()

        request.addHeader("x-api-key", apiKey)
        request.addHeader("Content-Type", "application/json")
        request.addHeader("Accept", "application/json")

        if (!accessToken.isNullOrBlank())
            request.addHeader("x-access-token", accessToken!!)

        return chain.proceed(request.build())
    }

    //    TODO handle 401 and interact user to re-authenticate - created call back to base activity
    override fun authenticate(route: Route?, response: Response): Request? {

        return if (response.code() != 200) {
            callback?.onUnAuthorizedResponse(response.code())
            null
        } else {
            response.request()
                .newBuilder()
                .build()
        }
    }

    fun setAuthCallBackListner(authenticatorCallBack: AuthenticatorCallBack) {
        callback = authenticatorCallBack
    }

    interface AuthenticatorCallBack {
        fun onUnAuthorizedResponse(responseCode: Int?)
    }
}
