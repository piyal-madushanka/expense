package com.madusanka.expenses.ui.gallery

import android.app.Application
import androidx.lifecycle.*
import com.madusanka.expenses.data.database.Expense
import com.madusanka.expenses.data.database.ExpenseDataBase
import com.madusanka.expenses.data.database.ExpenseRepository

class ShowExpenseViewModel(application: Application) : AndroidViewModel(application) {


    var readAllData: LiveData<List<Expense>>
    private var expenseRepository: ExpenseRepository


    init {
        val expenseDao = ExpenseDataBase.getDataBase(application).expenseDao()
        expenseRepository =  ExpenseRepository(expenseDao)
        readAllData = expenseRepository.findUser()
    }
}