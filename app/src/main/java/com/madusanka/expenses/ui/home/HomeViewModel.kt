package com.madusanka.expenses.ui.home

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import com.example.foody.data.Repository
import com.madusanka.expenses.data.database.Expense
import com.madusanka.expenses.data.database.ExpenseDataBase
import com.madusanka.expenses.data.database.ExpenseRepository
import com.madusanka.expenses.data.database.ExpenseResponse
import com.madusanka.expenses.utils.NetworkResult
import com.madusanka.expenses.utils.isNullOrEmpty
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Response
import java.lang.Exception

class HomeViewModel @ViewModelInject constructor(
    private val repository: Repository,
    application: Application
) : AndroidViewModel(application) {


    private val expenseDao = ExpenseDataBase.getDataBase(application).expenseDao()
    private var expenseRepository= ExpenseRepository(expenseDao)
    var expenseResponse: MutableLiveData<NetworkResult<ExpenseResponse>> = MutableLiveData()

    fun addUser(expense: Expense){
        viewModelScope.launch(Dispatchers.IO) {
            expenseRepository.addExpense(expense)
        }
    }


    @RequiresApi(Build.VERSION_CODES.M)
    fun getExpense(queries:String) = viewModelScope.launch {
        getExpenseSafeCall(queries)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private suspend fun getExpenseSafeCall(queries: String) {
        expenseResponse.value = NetworkResult.Loading()
        if (hasInternetConnection()) {
            try {
                val response = repository.remote.getExpense(queries)
                expenseResponse.value = handleExpenseResponse(response);

                val foodRecipe = expenseResponse.value!!.data
                if (foodRecipe != null) {
//                    offlineCacheRecipes(foodRecipe)
                }
            } catch (e: Exception) {
//                recipesResponse.value = NetworkResult.Error(e.toString())
                Log.e("network error -> ",e.toString())
            }
        } else {
            expenseResponse.value = NetworkResult.Error("No internet connection")
        }
    }



    private fun handleExpenseResponse(response: Response<ExpenseResponse>): NetworkResult<ExpenseResponse>? {
        when {
            response.message().toString().contains("timeout") -> {
                return NetworkResult.Error("Timeout");
            }
            response.code() == 402 -> {
                return NetworkResult.Error("API Key Limited.")
            }
            response.body()!!.results.isNullOrEmpty() as Boolean -> {
                return NetworkResult.Error("Recipes not found.")
            }
            response.isSuccessful -> {
                val expense = response.body();
                return NetworkResult.Success(expense!!)
            }
            else -> {
                return NetworkResult.Error(response.message())
            }
        }
    }


    @RequiresApi(Build.VERSION_CODES.M)
    private fun hasInternetConnection(): Boolean {
        val connectivityManager = getApplication<Application>().getSystemService(
            Context.CONNECTIVITY_SERVICE
        ) as ConnectivityManager
        val activeNetwork = connectivityManager.activeNetwork ?: return false
        val capabilities = connectivityManager.getNetworkCapabilities(activeNetwork) ?: return false
        return when {
            capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
            capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
            capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
            else -> false;
        }
    }

}


