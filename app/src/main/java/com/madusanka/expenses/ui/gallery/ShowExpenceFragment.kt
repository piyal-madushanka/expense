package com.madusanka.expenses.ui.gallery

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.madusanka.expenses.data.database.Expense
import com.madusanka.expenses.databinding.FragmentGalleryBinding

class ShowExpenceFragment : Fragment() {

    private lateinit var showExpenseViewModel: ShowExpenseViewModel
    private var _binding: FragmentGalleryBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        showExpenseViewModel = ViewModelProvider(this).get(ShowExpenseViewModel::class.java)

        _binding = FragmentGalleryBinding.inflate(inflater, container, false)
        val root: View = binding.root
        showExpenseViewModel.readAllData.observe(viewLifecycleOwner, Observer { setData(it) })

        return root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun setData(ex: List<Expense>) {
        if (ex.isNotEmpty()) {
            val expense = ex[0]
            binding.tvToday.text = expense.date
            binding.etTravel.text = expense.travel_amount.toString()
            binding.etFuel.text = expense.fuel_amount.toString()
            binding.etHighway.text = expense.highway_amount.toString()
            binding.etParking.text = expense.parking_amount.toString()
            binding.etVehicle.text = expense.vehicle_amount.toString()
            binding.etNight.text = expense.night_amount.toString()
            binding.etLunch.text = expense.lunch_amount.toString()
            binding.etTotal.text = expense.total_amount.toString()
            Log.e("log",expense.toString())
        }


    }

}