package com.madusanka.expenses.ui.home

import android.app.Activity.RESULT_OK
import android.app.DatePickerDialog
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.github.dhaval2404.imagepicker.ImagePicker
import com.madusanka.expenses.data.database.Expense
import com.madusanka.expenses.databinding.FragmentHomeBinding
import com.madusanka.expenses.utils.NetworkResult
import com.madusanka.expenses.utils.toJsonString
import com.madusanka.expenses.utils.validate
import dagger.hilt.android.AndroidEntryPoint
import java.text.SimpleDateFormat
import java.util.*

@AndroidEntryPoint
class HomeFragment : Fragment() {

    private lateinit var homeViewModel: HomeViewModel
    private var _binding: FragmentHomeBinding? = null

    var cal: Calendar = Calendar.getInstance()
    var travel_image: String = ""
    var fuel_image: String = ""
    var highway_image: String = ""
    var parking_image: String = ""
    var vehicle_Image: String = ""
    var night_Image: String = ""
    var lunch_Image: String = ""

    private val binding get() = _binding!!

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        homeViewModel = ViewModelProvider(requireActivity()).get(HomeViewModel::class.java)
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        binding.btnSave.setOnClickListener {
            if (isValidForm()) {
                addDataToDataBase()
            }

        }
        datePicker()
        updateDateInView()
        cameraFunctions()
        homeViewModel.expenseResponse.observe(viewLifecycleOwner, { response ->
            when (response) {
                is NetworkResult.Success -> {
                    response.data?.let {
                    Log.e("succsses",it.results.toString())
                    }
                }
                is NetworkResult.Error -> {
                    Toast.makeText(
                        requireContext(),
                        response.message.toString(),
                        Toast.LENGTH_SHORT
                    ).show()
                }
                is NetworkResult.Loading -> {

                }
            }
        })
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    fun getData(expense: Expense) {
        Log.e("fuck", "fuck")
        Log.e("expence", expense.toString())
    }

    private fun datePicker() {
        val dateSetListener =
            DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                cal.set(Calendar.YEAR, year)
                cal.set(Calendar.MONTH, monthOfYear)
                cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                updateDateInView()
            }

        binding.ivDate.setOnClickListener {
            val dialog = DatePickerDialog(
                requireContext(),
                dateSetListener,
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH)
            )
            dialog.datePicker.maxDate = Date().time
            dialog.show()
        }
    }

    private fun updateDateInView() {
        val myFormat = "EEEE , dd MMMM yyyy HH:mm a" // mention the format you need
        val sdf = SimpleDateFormat(myFormat, Locale.getDefault())
        binding.tvToday.text = (sdf.format(cal.time))
    }

    private fun cameraFunctions() {
        camDefault(binding.btnCamFuel, 1)
        camDefault(binding.btnCamNight, 2)
        camDefault(binding.btnCamLunch, 3)
        camDefault(binding.btnCamHighway, 4)
        camDefault(binding.btnCamParking, 5)
        camDefault(binding.btnCamTravel, 6)
        camDefault(binding.btnCamVehicle, 7)

    }

    private fun camDefault(imageView: ImageView, request: Int) {
        imageView.setOnClickListener {
            ImagePicker.with(this)
                .cameraOnly()    //User can only capture image using Camera
                .start(request)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            1 -> {
                when (resultCode) {
                    RESULT_OK -> {
                        val fileUri1 = data?.data
                        binding.ivCamFuel.setImageURI(fileUri1)
                        ImagePicker.with(this)
                        fuel_image = fileUri1.toString()
                    }
                    ImagePicker.RESULT_ERROR -> {
                        Toast.makeText(context, ImagePicker.getError(data), Toast.LENGTH_SHORT)
                            .show()
                    }
                    else -> {
                        Toast.makeText(context, "Task Cancelled", Toast.LENGTH_SHORT).show()
                    }
                }
            }
            2 -> {
                when (resultCode) {
                    RESULT_OK -> {
                        val fileUri = data?.data
                        binding.ivCamNight.setImageURI(fileUri)
                        ImagePicker.with(this)
                        night_Image = fileUri.toString()
                    }
                    ImagePicker.RESULT_ERROR -> {
                        Toast.makeText(context, ImagePicker.getError(data), Toast.LENGTH_SHORT)
                            .show()
                    }
                    else -> {
                        Toast.makeText(context, "Task Cancelled", Toast.LENGTH_SHORT).show()
                    }
                }
            }
            3 -> {
                when (resultCode) {
                    RESULT_OK -> {
                        val fileUri = data?.data
                        binding.ivCamLunch.setImageURI(fileUri)
                        ImagePicker.with(this)
                        lunch_Image = fileUri.toString()
                    }
                    ImagePicker.RESULT_ERROR -> {
                        Toast.makeText(context, ImagePicker.getError(data), Toast.LENGTH_SHORT)
                            .show()
                    }
                    else -> {
                        Toast.makeText(context, "Task Cancelled", Toast.LENGTH_SHORT).show()
                    }
                }
            }
            4 -> {
                when (resultCode) {
                    RESULT_OK -> {
                        val fileUri = data?.data
                        binding.ivCamHighway.setImageURI(fileUri)
                        ImagePicker.with(this)
                        highway_image = fileUri.toString()

                    }
                    ImagePicker.RESULT_ERROR -> {
                        Toast.makeText(context, ImagePicker.getError(data), Toast.LENGTH_SHORT)
                            .show()
                    }
                    else -> {
                        Toast.makeText(context, "Task Cancelled", Toast.LENGTH_SHORT).show()
                    }
                }
            }
            5 -> {
                when (resultCode) {
                    RESULT_OK -> {
                        val fileUri = data?.data
                        binding.ivCamParking.setImageURI(fileUri)
                        ImagePicker.with(this)
                        parking_image = fileUri.toString()

                    }
                    ImagePicker.RESULT_ERROR -> {
                        Toast.makeText(context, ImagePicker.getError(data), Toast.LENGTH_SHORT)
                            .show()
                    }
                    else -> {
                        Toast.makeText(context, "Task Cancelled", Toast.LENGTH_SHORT).show()
                    }
                }
            }
            6 -> {
                when (resultCode) {
                    RESULT_OK -> {
                        val fileUri = data?.data
                        binding.ivCamTravel.setImageURI(fileUri)
                        ImagePicker.with(this)
                        travel_image = fileUri.toString()

                    }
                    ImagePicker.RESULT_ERROR -> {
                        Toast.makeText(context, ImagePicker.getError(data), Toast.LENGTH_SHORT)
                            .show()
                    }
                    else -> {
                        Toast.makeText(context, "Task Cancelled", Toast.LENGTH_SHORT).show()
                    }
                }
            }
            7 -> {
                when (resultCode) {
                    RESULT_OK -> {
                        val fileUri = data?.data
                        binding.ivCamVehicle.setImageURI(fileUri)
                        ImagePicker.with(this)
                        vehicle_Image = fileUri.toString()

                    }
                    ImagePicker.RESULT_ERROR -> {
                        Toast.makeText(context, ImagePicker.getError(data), Toast.LENGTH_SHORT)
                            .show()
                    }
                    else -> {
                        Toast.makeText(context, "Task Cancelled", Toast.LENGTH_SHORT).show()
                    }
                }
            }

        }
    }

    private fun isValidForm(): Boolean {

        val travelEx =
            binding.etTravel.validate("valid payment value required") { s -> s.isNotEmpty() }
        val fuelEx = binding.etFuel.validate("valid payment value required") { s -> s.isNotEmpty() }
        val highwayEx =
            binding.etHighway.validate("valid payment value required") { s -> s.isNotEmpty() }
        val parkingEx =
            binding.etParking.validate("valid payment value required") { s -> s.isNotEmpty() }
        val vehicleEx =
            binding.etVehicle.validate("valid payment value required") { s -> s.isNotEmpty() }
        val nightEx =
            binding.etNight.validate("valid payment value required") { s -> s.isNotEmpty() }
        val lunchEx =
            binding.etLunch.validate("valid payment value required") { s -> s.isNotEmpty() }

        return travelEx && fuelEx && highwayEx && parkingEx && vehicleEx && nightEx && lunchEx
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun addDataToDataBase() {
        val travelEx = binding.etTravel.text.toString()
        val fuelEx = binding.etFuel.text.toString()
        val highwayEx = binding.etHighway.text.toString()
        val parkingEx = binding.etParking.text.toString()
        val vehicleEx = binding.etVehicle.text.toString()
        val nightEx = binding.etNight.text.toString()
        val lunchEx = binding.etLunch.text.toString()
        val total =
            (travelEx.toInt() + fuelEx.toInt() + highwayEx.toInt() + parkingEx.toInt() + vehicleEx.toInt() + nightEx.toInt() + lunchEx.toInt())
        val expense = Expense(
            1,
            binding.tvToday.text.toString(),
            travelEx.toInt(),
            fuelEx.toInt(),
            highwayEx.toInt(),
            parkingEx.toInt(),
            vehicleEx.toInt(),
            nightEx.toInt(),
            lunchEx.toInt(),
            total,
            travel_image,
            fuel_image,
            highway_image,
            parking_image,
            vehicle_Image,
            night_Image,
            lunch_Image
        )

        homeViewModel.addUser(expense)
        homeViewModel.getExpense(expense.toJsonString())

        Toast.makeText(context, "Record added successfully", Toast.LENGTH_SHORT).show()


    }
}