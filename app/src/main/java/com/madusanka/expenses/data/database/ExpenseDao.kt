package com.madusanka.expenses.data.database

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface ExpenseDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun addExpense(expense: Expense)

    @Update
    suspend fun updateUser(expense: Expense)

    @Delete
    suspend fun deleteUser(expense: Expense)

    @Query("SELECT * FROM expense_table ORDER BY  id ASC")
    fun findByUserId(): LiveData<List<Expense>>
}