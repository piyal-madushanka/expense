package com.madusanka.expenses.data.database

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable


@Entity(tableName = "expense_table")
data class Expense(
    @PrimaryKey(autoGenerate = false)
    var id: Int? = 0,
    var date: String = "",
    var travel_amount: Int = 0,
    var fuel_amount: Int = 0,
    var highway_amount: Int = 0,
    var parking_amount: Int = 0,
    var vehicle_amount: Int = 0,
    var night_amount: Int = 0,
    var lunch_amount: Int = 0,
    var total_amount: Int = 0,
    var travel_image: String = "",
    var fuel_image: String = "",
    var highway_image: String = "",
    var parking_image:String = "",
    var vehicle_Image:String = "",
    var night_Image:String = "",
    var lunch_Image:String = ""
) : Serializable
