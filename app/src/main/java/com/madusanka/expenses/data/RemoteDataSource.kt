package com.madusanka.expenses.data

import com.madusanka.expenses.data.database.ExpenseResponse
import com.madusanka.expenses.data.network.ExpenseApi
import retrofit2.Response
import javax.inject.Inject

class RemoteDataSource @Inject constructor(
    private val expenseApi: ExpenseApi
) {

    suspend fun getExpense(queries: String): Response<ExpenseResponse> {
        return expenseApi.getRecipes(queries);
    }

}