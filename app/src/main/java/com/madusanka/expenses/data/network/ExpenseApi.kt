package com.madusanka.expenses.data.network

import com.madusanka.expenses.data.database.ExpenseResponse
import retrofit2.Response
import retrofit2.http.POST

interface ExpenseApi {

    @retrofit2.http.FormUrlEncoded
    @POST("/testapi.php")
    suspend fun getRecipes(
        @retrofit2.http.Field("jsonString") jsonString: String
    ): Response<ExpenseResponse>

}
