package com.madusanka.expenses.data.database

import com.google.gson.annotations.SerializedName

data class ExpenseResponse(
    @SerializedName("results")
    val results: Any
)