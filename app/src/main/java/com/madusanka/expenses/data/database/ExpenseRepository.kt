package com.madusanka.expenses.data.database

import androidx.lifecycle.LiveData

class ExpenseRepository(private val expenseDao: ExpenseDao) {

    fun findUser(): LiveData<List<Expense>> {
        return expenseDao.findByUserId()
    }

    suspend fun addExpense(expense: Expense){
        expenseDao.addExpense(expense)
    }

    suspend fun updateUser(expense: Expense){
        expenseDao.updateUser(expense)
    }

    suspend fun deleteUser(expense: Expense){
        expenseDao.deleteUser(expense)
    }
}