package com.madusanka.expenses.utils

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import kotlin.contracts.ExperimentalContracts
import kotlin.contracts.contract

fun <T> LiveData<T>.observeOnce(lifecycleOwner: LifecycleOwner, observer: Observer<T>) {
    observe(lifecycleOwner, object : Observer<T> {
        override fun onChanged(t: T?) {
            removeObserver(this)
            observer.onChanged(t)
        }
    })
}

@ExperimentalContracts
public inline fun <T> Collection<T>?.isNullOrEmpty(): Boolean {
    contract {
        returns(false) implies (this@isNullOrEmpty != null)
    }

    return this == null || this.isEmpty()

}
inline fun <reified T : Any> Any.mapTo(): T =
    GsonBuilder().create().run {
        toJson(this@mapTo).let { fromJson(it, T::class.java) }
    }

inline fun <reified T : Any> String.jsonStringMapTo(): T =
    Gson().fromJson(this@jsonStringMapTo, T::class.java)

fun Any.toJsonString(): String = Gson().toJson(this@toJsonString)

fun Any.isNullOrEmpty(): Any {
    this.isNullOrEmpty()
    return true
}