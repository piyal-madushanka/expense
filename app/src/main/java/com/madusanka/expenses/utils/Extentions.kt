package com.madusanka.expenses.utils

import android.content.Context
import android.content.res.Resources
import android.graphics.Typeface
import android.os.Build
import android.text.Editable
import android.text.TextWatcher
import android.util.TypedValue
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.ActionBar
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat

/**
 * You can validate the EditText like
 *
 * et_email.validate("Valid email address required"){ s -> s.isValidEmail() }
 *
 * et_email.validate("Minimum length is 6"){ s-> s.length>=6 }
 *
 * */
fun EditText.validate(message: String, validator: (String) -> Boolean): Boolean {
    this.afterTextChanged {
        this.error = if (validator(it)) null else message
    }
    this.error = if (validator(this.getString())) null else message

    return validator(this.getString())
}
fun EditText.afterTextChanged(afterTextChanged: (String) -> Unit) {
    this.addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
            afterTextChanged.invoke(s.toString().trim())
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
    })
}
fun EditText.getString(): String {
    return this.text.toString()
}
fun ActionBar.setActionBarMain(
    context: Context,
    title: String,
    isHomeUpEnables: Boolean = false,
    textSize: Float,
    textColor: Int

) {


    val tv = TextView(context.applicationContext)

// Create a LayoutParams for TextView
    val lp = ActionBar.LayoutParams(
        ActionBar.LayoutParams.WRAP_CONTENT, // Width of TextView
        ActionBar.LayoutParams.WRAP_CONTENT
    ) // Height of TextView

// Apply the layout parameters to TextView widget
// tv.layoutParams = lp
    lp.gravity = Gravity.CENTER

// Set text to display in TextView
    tv.text = title // ActionBar title text

//set font family
    tv.typeface = Typeface.DEFAULT_BOLD
//    val myCustomFont : Typeface? = ResourcesCompat.getFont(context, fontFamily)
//    tv.typeface = myCustomFont


//change text color
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        tv.setTextColor(ContextCompat.getColor(context, textColor));
    }else{
        tv.setTextColor(context.resources.getColor(textColor))
    }
    tv.typeface
// if (isCenterTitle) {
// tv.gravity = Gravity.CENTER_HORIZONTAL
// }
// Set the TextView text size in dp
// This will change the ActionBar title text size
    tv.setTextSize(TypedValue.COMPLEX_UNIT_DIP, textSize)

// Set the ActionBar display option
    this.displayOptions = ActionBar.DISPLAY_SHOW_CUSTOM
    this.setCustomView(tv, lp)
//    this.setBackgroundDrawable(
//        ContextCompat.getDrawable(
//            context,
//            toolbarBackground
//        )
//    )

    this.elevation = 1.dpToPx()
    if (isHomeUpEnables) {
        this.setDisplayHomeAsUpEnabled(true)
        this.setDisplayShowHomeEnabled(true)
// Todo add back drawable here
//        this.setHomeAsUpIndicator(backButtonDrawable)
    }

}

fun Int.dpToPx(): Float {
    val density = Resources.getSystem()
        .displayMetrics.density
    return this * density
}
fun View.margin(left: Float? = null, top: Float? = null, right: Float? = null, bottom: Float? = null) {
    layoutParams<ViewGroup.MarginLayoutParams> {
        left?.run { leftMargin = dpToPx(this) }
        top?.run { topMargin = dpToPx(this) }
        right?.run { rightMargin = dpToPx(this) }
        bottom?.run { bottomMargin = dpToPx(this) }
    }
}

inline fun <reified T : ViewGroup.LayoutParams> View.layoutParams(block: T.() -> Unit) {
    if (layoutParams is T) block(layoutParams as T)
}

fun View.dpToPx(dp: Float): Int = context.dpToPx(dp)
fun Context.dpToPx(dp: Float): Int = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, resources.displayMetrics).toInt()